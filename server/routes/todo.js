const express = require('express');
const router = express.Router();
const mongodb = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const uri = 'mongodb://test:abc12345@ds119070.mlab.com:19070/duynam';


router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended:true
}));

router.get('/', (req, res) => {
  // console.log(req.app.io);
  let d = new Date();
  req.app.io.emit("listRetrieved",`Got list at ${d.toDateString()} ${d.getHours()}: ${d.getMinutes()} : ${d.getSeconds()}`)
  mongodb.connect(uri, { useNewUrlParser: true },(err,client)=>{
    if (err) {
      throw err
    }
    const db = client.db('duynam');
    db.collection('todo').find()
                        .toArray((error,result)=>{
                              if (error){
                                throw error;
                              }
                              res.send(result);
                        })
  });
});

router.get('/:id', (req, res) => {
  mongodb.connect(uri, { useNewUrlParser: true },(err,client)=>{
    if (err) {
      throw err
    }
    const db = client.db("duynam");
    const ObjectId = require("mongodb").ObjectID;

    db.collection('todo').find({"_id":ObjectId(req.params.id)})
                        .toArray((error,result)=>{
                              if (error){
                                throw error;
                              }
                              res.send(result);
                        })
  });
});

router.post('/',(req,res)=>{
  let d = new Date();
  req.app.io.emit("listRetrieved",`Posted ${req.body.entry} at ${d.toDateString()} ${d.getHours()}: ${d.getMinutes()} : ${d.getSeconds()}`)

  mongodb.connect(uri,{useNewUrlParser:true},(err,client)=>{
    if(err){
      throw err;
    }
    const db = client.db('duynam');
    db.collection('todo').insertOne({entry:req.body.entry},(error,q)=>{
      if(error){
        throw error;
      }
      res.send(q);
    });
  })
})

router.put('/:id',(req,res)=>{
  let {id} = req.params;
  let d = new Date();
  req.app.io.emit("listRetrieved",`Modified ${id} at ${d.toDateString()} ${d.getHours()}: ${d.getMinutes()} : ${d.getSeconds()}`)
  mongodb.connect(uri,{useNewUrlParser:true},(err,client)=>{
    if(err){
      throw err;
    }
    const db = client.db('duynam');
    const ObjectId = require('mongodb').ObjectID;
    db.collection('todo').updateOne({ "_id": ObjectId(id)}
                                   ,{$set : {"entry":req.body.text}}
                                   ,{upsert:true}
                                   ,(error,q)=>{
      if(error){
         throw error;
      }
      res.send(q);
    });
  })
});

router.delete("/:id",(req,res)=>{
  let {id} = req.params;
  let d = new Date();
  req.app.io.emit("listRetrieved",`Deleted ${id} at ${d.toDateString()} ${d.getHours()}: ${d.getMinutes()} : ${d.getSeconds()}`)
  mongodb.connect(uri,{useNewUrlParser:true},(err,client)=>{
    if(err){
      throw err;
    }
    const db = client.db("duynam");
    const ObjectId = require("mongodb").ObjectID;
    db.collection("todo").deleteOne({"_id":ObjectId(id)}
                                    ,(error,q)=>{
                                      if(error) {
                                        throw error
                                      }
                                      res.send(q);
                                    }
    );
  });
});
module.exports = router;
