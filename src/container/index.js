import Todo from '../component/todo/Todo.js';
import { connect } from 'react-redux'
import {
    updateEntry,
    changeTodoEntryState,
    updateModifyInput,
    undoChange
} from '../action'
// import {modifyTodoAsync} from '../action/action-modify';
import {requestAddTodoEpic} from '../epics/action.todo.add';
import {REQUEST_TODO_DELETE_EPIC_SENT} from '../epics/action.delete';
import {REQUEST_MODIFY_TODO_EPIC_SENT} from '../epics/action.modify';

import {subscribeToSocketConnection} from '../socket';
const mapStateToProps = (state) => ({
  todo:state
})

const mapDispatchToProps = (dispatch) => {
    return{
        onLoad:()=>{
            subscribeToSocketConnection(dispatch)
        },
        // initiateSocketConnection:()=>{
        //     // dispatch(socketIoConnect());
        // },
        dispatchChange:(text)=>{
            dispatch(updateEntry(text));
        },
        dispatchClick:()=>{
            dispatch(requestAddTodoEpic());
            // dispatch(postToDoAsync(text));
        },
        removeTodo:(id,extRef)=>{
            dispatch({type:REQUEST_TODO_DELETE_EPIC_SENT,id:extRef})
            // dispatch(deleteTodoAsync(id,extRef));
        },
        changeEntryState:(id,entryState)=>{
            dispatch(changeTodoEntryState(id,entryState))
        },
        onChangeModifyInput:(id,text)=>{
            dispatch(updateModifyInput(id,text))
        },
        modifyTodo:(id,extRef,newText)=>{
            dispatch({type:REQUEST_MODIFY_TODO_EPIC_SENT,id:extRef,text:newText})
            // dispatch(modifyTodoAsync(id,extRef,newText))
        },
        undoChange:(id)=>{
            dispatch(undoChange(id))
        }
    }
}

export const TodoContainer = connect(
    mapStateToProps
    ,mapDispatchToProps
)(Todo)