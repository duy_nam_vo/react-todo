// import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ConnectionStatus from '../component/connection/connectionStatus';

const mapStateToProps = (state) => ({
    connection : state.AppConnection
});
export const ConnectionContainer = connect(
    mapStateToProps
)(ConnectionStatus)
// export default connect(mapStateToProps)(container-connection)
