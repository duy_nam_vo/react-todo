export const entryState = Object.freeze({
    READ: 1,
    MODIFY:2,
    MODIFIED:3,
    DELETED:4
})
export const todoState = Object.freeze({
    IDLE:1,
    FETCHING:2,
    FETCHED:3,
    ERROR:4
})
