import configureStore from './configureStore';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Root from './component/todo/Root';

const store = configureStore()

ReactDOM.render(
<Root store={store} />
, document.getElementById('root'));
registerServiceWorker();
