export const UPDATE_ENTRY = 'UPDATE_ENTRY';
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const MODIFY_TODO = 'MODIFY_TODO';
export const CHANGE_TODO_ENTRY_STATE = 'CHANGE_TODO_ENTRY_STATE';
export const UPDATE_MODIFY_INPUT = 'UPDATE_MODIFY_INPUT';
export const UNDO_CHANGE = 'UNDO_CHANGE'

// Data fetching
export const REQUEST_TODO = 'REQUEST_TODO';
export const RECEIVE_TODO = 'RECEIVE_TODO'

