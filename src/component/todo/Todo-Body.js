import React, { Component } from 'react';
import TodoElement from './Todo-Element.js';
import {todoState} from '../../static/entryState.js';
import {RingLoader} from 'react-spinners';
import TodoElementError from './Todo-Element-Error'
class TodoBody extends Component {

  render() {
    return (
      <div>
        {this.props.AppTodo.todoState === todoState.ERROR && <TodoElementError error = {this.props.AppTodo.error}/>}
        {this.props.AppTodo.todoState===todoState.FETCHING && 
          <RingLoader />}
        <ul>
            {this.props.AppTodo.entries.length > 0 && this.props.AppTodo.entries.map((elem,i)=><TodoElement id={i} key={i} entry={elem} {...this.props}/>)}
        </ul>
      </div>
    )
  }
}

export default TodoBody

