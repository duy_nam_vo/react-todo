import React from 'react';
import Footer from '../../static/Footer';
import {Provider} from 'react-redux';
import {TodoContainer} from '../../container/index';
import Header from '../../static/Header';
// import ConnectionStatus from '../connection/connectionStatus';
import {ConnectionContainer} from '../../container/container-connection';
const App = ()=>(
    <div id="container">
        <Header />
        <ConnectionContainer />
        <TodoContainer />
        <Footer />
    </div>
);

const Root = ({store})=>(
    <Provider store = {store}>
    <App />
    </Provider>
)

export default Root;