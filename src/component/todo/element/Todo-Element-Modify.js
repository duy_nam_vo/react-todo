import React, { Component } from 'react'
import Container from 'muicss/lib/react/container';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import Button from 'muicss/lib/react/button';
import Input from 'muicss/lib/react/input';
import {entryState} from '../../../static/entryState'

class TodoElementModify extends Component{
    render(){
        return(
            <Container fluid={true}>
              <Row>
                  <Col md="7"><Input placeholder="modify" value={this.props.entry.modifyInput} onChange={(ev)=>this.props.onChangeModifyInput(this.props.id,ev.target.value)}/></Col>
                  <Col md="2"><Button color="primary" onClick={()=>this.props.onModify(this.props.id,this.props.entry.id,this.props.entry.modifyInput)}>Save</Button></Col>
                  
                  <Col md="2"><Button color="danger" onClick={()=>this.props.onChangeEntryState(this.props.id,entryState.READ)}>Cancel</Button></Col>
                  {this.props.entry.state === entryState.MODIFIED && 
                  <Col md="2"><Button color="accent" onClick={()=>this.props.undoChange(this.props.id)}>Undo</Button></Col>}
              </Row>
            </Container>
        )
    }
}

export default TodoElementModify;