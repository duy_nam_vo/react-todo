import React, { Component } from 'react'
import Container from 'muicss/lib/react/container';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import Button from 'muicss/lib/react/button';
import {entryState} from '../../../static/entryState'

class TodoElementRead extends Component{
    render(){
        return(
            <Container fluid={true}>
                <Row>
                    <Col md="7">{this.props.entry.entry}</Col>
                    <Col md="2"><Button color="primary" onClick={()=>this.props.onChangeEntryState(this.props.id,entryState.MODIFY)}>Modify</Button></Col>
                    <Col md="2"><Button color="danger" onClick={()=>this.props.onRemove(this.props.id,this.props.entry.id)}>Remove</Button></Col>
                </Row>
            </Container>
        )
    }
}
export default TodoElementRead;