import React, { Component } from 'react'
import './style/todo.css';
import TodoEntryForm from './Todo-EntryForm.js'
export default class Todo extends Component {
  constructor(props){
    super(props);
    this.submitEntry = this.submitEntry.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }
  componentDidMount(){
    this.props.onLoad();
  }
  onKeyPress(event){
    if (event.key==='Enter'){
      this.submitEntry(event);
    }
  }
  submitEntry(ev){
    ev.preventDefault();
    const {input} = this.props.todo.AppTodo
    if (input !== ""){
      this.props.dispatchClick(input);
    }
  }
  
  render() {
    return (
      <div id="todo">
          {/* <h1>TODO list </h1> */}
          <TodoEntryForm onChange={this.props.dispatchChange}
                         onClick={this.submitEntry}
                         onRemove={this.props.removeTodo}
                         onChangeEntryState={this.props.changeEntryState}
                         onChangeModifyInput={this.props.onChangeModifyInput}
                         onModify={this.props.modifyTodo}
                         undoChange={this.props.undoChange}
                         onKeyPress={this.onKeyPress}
                         {...this.props.todo}
                         />
      </div>
    )
  }
}
