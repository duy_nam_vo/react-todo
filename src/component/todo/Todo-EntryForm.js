import React, { Component } from 'react'
import TodoBody from './Todo-Body.js'
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';

export default class TodoEntryForm extends Component {
  
  render() {
    return (
      <div>
        <h2>Enter note</h2>

         <Container fluid={true}>
              <Row>
                  < Col md = "5" > < Input placeholder = "To do"
                  value = { this.props.AppTodo.input }
                  onChange = { (ev) => this.props.onChange(ev.target.value) }
                  onKeyPress = {this.props.onKeyPress}
                  /></Col >
                  <Col md="2"><Button variant="fab" onClick={this.props.onClick}>+</Button></Col>
                  {/* <Col md="2"><Button variant="fab" onClick={this.props.onTest}>TEST</Button></Col> */}
              </Row>
             
        </Container> 
        <TodoBody {...this.props} />
       
        
        
      </div>
      
    )
  }
}
