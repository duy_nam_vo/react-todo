import React, { Component } from 'react'
// import Container from 'muicss/lib/react/container';
// import Row from 'muicss/lib/react/row';
// import Col from 'muicss/lib/react/col';
// import Button from 'muicss/lib/react/button';
// import Input from 'muicss/lib/react/input';
import {entryState} from '../../static/entryState.js';
import './style/todoElement.css';
import TodoElementModify from './element/Todo-Element-Modify';
import TodoElementRead from './element/Todo-Element-Read';
export default class TodoElement extends Component {
  render() {
    return (
        
        <li>
            {this.props.entry.state !== entryState.READ &&
            <TodoElementModify {...this.props} />

            }
            {this.props.entry.state === entryState.READ && 
                <TodoElementRead {...this.props} />
               
            }
        </li>
    )
  }
}
