import React from 'react';
import MaterialIcon,{colorPalette} from 'material-icons-react';
import './style/todoError.css';

const TodoElementError = (props)=>    
    <div className="todoError"> 
        <MaterialIcon icon="error" color={colorPalette.red._700} />

        <p >{props.error}</p>
    </div>


export default TodoElementError;