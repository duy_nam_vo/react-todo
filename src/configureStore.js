import rootReducer from './reducer/index.js';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { saveState } from './localStorage.js';
import { throttle } from 'lodash';
import rootEpic from './epics/root';
import {createEpicMiddleware} from 'redux-observable';
const configureStore = () => {
    // const persistedState = loadState();
    
    const loggerMiddleware = createLogger();
    const epicMiddleWare = createEpicMiddleware()
    const store = createStore(rootReducer, {
        // AppTodo: persistedState.AppTodo
        undefined
    }, applyMiddleware(thunkMiddleware, loggerMiddleware,epicMiddleWare));
    epicMiddleWare.run(rootEpic);
    store.subscribe(throttle(() => {
        saveState(store.getState());
    }, 1000));

    return store;
}

export default configureStore;