import fetch from 'cross-fetch';

const methods = {
    mode: "cors", 
    cache: "no-cache", 
    credentials: "same-origin", 
    headers: {
        "Content-Type": "application/json; charset=utf-8"
    },
    redirect: "follow", 
    referrer: "no-referrer"
}

export const postTodo = (data={})=>{
    return fetch(`/todo`,
        Object.assign(methods,{method:"POST",body:JSON.stringify(data)})
    )
    .then(response=>{
        if (response.ok){
            return response.json();
        }
        throw response;
    })
}

export const getTodo = ()=>{
    return fetch(`/todo`,
        Object.assign(methods,{method:"GET"})
    ).then(response=>{
        if(response.ok){
            return response.json()
        } 
        throw response})
}
export const modifyTodoData = (id,text)=>{
    return fetch(`/todo/${id}`,
        Object.assign(methods,{method:"PUT",body:JSON.stringify({"text":text})})
    )
    .then(response=>{
        if (response.ok){
            return response.json();
        }
        throw response;
    })
}

export const deleteTodoData = (id)=>{
    return fetch(`/todo/${id}`,
    Object.assign(methods,{method:"DELETE"})
    ).then(response=>{
        if(response.ok){
            // console.log(response.json());
            return response.json();
        }
        throw response;
    })
}