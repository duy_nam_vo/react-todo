import openSocket from 'socket.io-client';
import {CONNECT_RECEIVED_EPIC,DISCONNECT_RECEIVED_EPIC} from '../epics/action.connection';
const socket = openSocket("http://localhost:5000",{reconnection:true,reconnectionDelay:3000});

export const subscribeToSocketConnection = (dispatch)=>{
    // socket.emit("subscribeToConnection");
    socket.on("connected",()=>{
        dispatch({type:CONNECT_RECEIVED_EPIC})
    })
    socket.on("disconnect",()=>{
        dispatch({type:DISCONNECT_RECEIVED_EPIC})
    })
}