import {REQUEST_ADD_ERROR,REQUEST_ADD_SENT,REQUEST_ADD_RECEIVED} from '../actionType/crossSlice';
import {postTodo} from '../data/dataAccessLayer';
import {addTodo} from '../action';

export const requestAddSent = (id) => ({
    type: REQUEST_ADD_SENT,
    id:id
  });
  export const requestAddReceived = (data) =>({
    type: REQUEST_ADD_RECEIVED,
    data:data
  })
  export const requestAddError = (error) =>({
    type:REQUEST_ADD_ERROR,
    error : error
  })

  export function postToDoAsync(text){
    return async function(dispatch){
        try{
            dispatch(requestAddSent());
            await postTodo({"entry":text})
            dispatch(requestAddReceived())
            dispatch(addTodo())

        }catch(error){
            dispatch(requestAddError(`${error.status}:${error.statusText}`))
            // dispatch(requestError(`${error.status}:${error.statusText}`))
        }
    }
  }