import {ADD_TODO,
  UPDATE_ENTRY,
  REMOVE_TODO,
  MODIFY_TODO,
  CHANGE_TODO_ENTRY_STATE,
  UPDATE_MODIFY_INPUT,
  UNDO_CHANGE} from '../actionType';

export const addTodo = () => ({
  type: ADD_TODO
})

export const updateEntry = (text) => ({
  type: UPDATE_ENTRY,
  text:text
})

export const removeTodo = (id)=>({
  type: REMOVE_TODO,
  id:id
})

export const modifyTodo = (id)=>({
  type: MODIFY_TODO,
  id:id
})

export const changeTodoEntryState =(id,entryState)=>({
  type: CHANGE_TODO_ENTRY_STATE,
  id:id,
  newState: entryState
})

export const updateModifyInput=(id,text)=>({
  type:UPDATE_MODIFY_INPUT,
  id:id,
  text:text
})

export const undoChange=(id)=>({
  type:UNDO_CHANGE,
  id:id
})

