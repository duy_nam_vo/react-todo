import { COMBINED_TEST, REQUEST_SENT,REQUEST_RECEIVED,REQUEST_ERROR } from "../actionType/crossSlice.js";
import {getTodo} from '../data/dataAccessLayer';

export const combinedTest = () => ({
  type: COMBINED_TEST
});
export const requestSent = () => ({
  type: REQUEST_SENT
});
export const requestReceived = (data) =>({
  type: REQUEST_RECEIVED,
  data:data
})
export const requestError = (error) =>({
  type:REQUEST_ERROR,
  error : error
})

export function fetchTodo(){

  return async function(dispatch){
      try{
        dispatch(requestSent());
        const resp = await getTodo();
        resp.json()
        .then(json=>
          dispatch(requestReceived(json))
        )
      }catch(error){
        dispatch(requestError(`${error.status}:${error.statusText}`))
      }
  }
}