import {CONNECTION_RECEIVED} from '../actionType/connection';
import openSocket from 'socket.io-client';
const socket = openSocket("http://localhost:5000");

export const connectionReceived = ()=>({
    type:CONNECTION_RECEIVED
})

export function socketIoConnect(){
    return function(dispatch){
        socket.emit("subscribeToConnection",()=>{
            dispatch(connectionReceived());
        });
    }
}