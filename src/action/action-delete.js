import { REQUEST_DELETE_ERROR,REQUEST_DELETE_SENT,REQUEST_DELETE_RECEIVED } from "../actionType/crossSlice.js";
import {deleteTodoData} from '../data/dataAccessLayer';
import {removeTodo} from '../action';

export const requestDeleteSent = (id) => ({
    type: REQUEST_DELETE_SENT,
    id:id
  });
  export const requestDeleteReceived = (id) =>({
    type: REQUEST_DELETE_RECEIVED,
    id:id
  })
  export const requestDeleteError = (error) =>({
    type:REQUEST_DELETE_ERROR,
    error : error
  })

  export function deleteTodoAsync(id,extRef){
    return async function(dispatch){
        try{
            dispatch(requestDeleteSent(id));
            await deleteTodoData(extRef)
            dispatch(removeTodo(id))
            dispatch(requestDeleteReceived(id))

        }catch(error){
            dispatch(requestDeleteError(`${error.status}:${error.statusText}`))
        }
    }
  }