import { REQUEST_MODIFY_ERROR,REQUEST_MODIFY_SENT,REQUEST_MODIFY_RECEIVED } from "../actionType/crossSlice.js";
import {modifyTodoData} from '../data/dataAccessLayer';
import {modifyTodo} from '../action';

export const requestModifySent = (id) => ({
    type: REQUEST_MODIFY_SENT,
    id:id
  });
  export const requestModifyReceived = (id) =>({
    type: REQUEST_MODIFY_RECEIVED,
    id:id
  })
  export const requestModifyError = (error) =>({
    type:REQUEST_MODIFY_ERROR,
    error : error
  })

  export function modifyTodoAsync(id,extRef,newText){
    return async function(dispatch){
        try{
            dispatch(requestModifySent(id));
            await modifyTodoData(extRef,newText)
            dispatch(modifyTodo(id))
            dispatch(requestModifyReceived(id))

        }catch(error){
            dispatch(requestModifyError(`${error.status}:${error.statusText}`))
        }
    }
  }