import { ofType } from 'redux-observable';
import {mapTo} from 'rxjs/operators'
// import {from} from 'rxjs';


export const CONNECT_SOCKET_EPIC = "CONNECT_SOCKET_EPIC";
export const CONNECT_RECEIVED_EPIC = "CONNECT_RECEIVED_EPIC";
export const DISCONNECT_RECEIVED_EPIC = "DISCONNECT_RECEIVED_EPIC";

export const connectToSocketIoEpic = action$ => {
    // console.log(action$.type);
    return action$.pipe(
        ofType(CONNECT_RECEIVED_EPIC),
        mapTo({type:"REQUEST_TODO_EPIC"})
    )
}

// export const listenToStateEpic = (action$,store$) =>{ 

//     console.log(store$.value.AppConnection)
//     return store$.pipe(
//         // map(store=>{
//         //     console.log(store)
//         //     return store.value
//         // }),
//         filter(val=>{
//             console.log(val.AppConnection)
//             return val.AppConnection.connectionStatus === 1 && val.AppConnection.connectReceivedToState === 0
//         }),
//         mapTo({type:"CONNECT_RECEIVED_TO_STATE"})
        
//     // filter((store$) => store$.value.AppConnection.connectionStatus === 1),
//     // mapTo({type:"CONNECT_RECEIVED_TO_STATE"})
//     )
//     // return action$.ofType("CONNECT_RECEIVED_TO_STATE")
// }