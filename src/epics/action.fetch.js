import {mergeMap,flatMap} from 'rxjs/operators';
import {api} from './api'
import { ofType } from 'redux-observable';
import {from} from 'rxjs';

export const REQUEST_TODO_EPIC="REQUEST_TODO_EPIC";
export const REQUEST_RECEIVED_EPIC = "REQUEST_RECEIVED_EPIC"
export const REQUEST_FETCH_FULFILLED_EPIC = "REQUEST_FETCH_FULFILLED_EPIC"

export const requestTodoEpic =()=>({
    type:REQUEST_TODO_EPIC
})
const requestReceivedEpic=()=>({
    type:REQUEST_RECEIVED_EPIC
});

const requestFetchFulfilledEpic=(data,state)=>({
    type:REQUEST_FETCH_FULFILLED_EPIC,
    data:data,
    contentState : state
});

export const fetchTodoEpic = (action$,state$)=> action$.pipe(
    ofType(REQUEST_TODO_EPIC),
    mergeMap(()=>api.getTodoEpic().pipe(
        flatMap(response=>{
            // console.log(response);
            let actions = [requestFetchFulfilledEpic(response,state$.value.AppTodo),requestReceivedEpic()]
            return from(actions);
        })
    )
    )
);
