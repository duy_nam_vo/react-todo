import {mergeMap,flatMap} from 'rxjs/operators';
import {api} from './api'
import { ofType } from 'redux-observable';
import {from} from 'rxjs';
import {ADD_TODO} from '../actionType/index';

export const REQUEST_ADD_TODO_EPIC="REQUEST_ADD_TODO_EPIC";
export const REQUEST_ADD_TODO_SENT_EPIC="REQUEST_ADD_TODO_SENT_EPIC";
export const REQUEST_ADD_TODO_FULFILLED_EPIC = "REQUEST_ADD_FULFILLED_EPIC";
export const ADD_TODO_EPIC = "ADD_TODO_EPIC";

export const requestAddTodoEpic = ()=>({
    type:REQUEST_ADD_TODO_EPIC
})

export const addTodoEpic =(action$, state$)=>action$.pipe(
    ofType(REQUEST_ADD_TODO_EPIC),
    mergeMap(()=>api.postTodoEpic(state$.value.AppTodo.input).pipe(
            flatMap(()=>{
                let actions = [{type:ADD_TODO},{type:REQUEST_ADD_TODO_FULFILLED_EPIC}]
                return from(actions);
            })
        )
    )

);