import { ofType } from "redux-observable";
import {mergeMap,flatMap} from 'rxjs/operators';
import { from } from "rxjs";
import { api } from "./api";
import {MODIFY_TODO} from '../actionType'; 
export const REQUEST_MODIFY_TODO_EPIC_SENT = "REQUEST_MODIFY_TODO_EPIC_SENT"
export const REQUEST_MODIFY_TODO_FULFILLED_EPIC = "REQUEST_MODIFY_TODO_FULFILLED_EPIC"

export const modifyTodoEpic = (action$) => 


    action$.pipe(
        ofType(REQUEST_MODIFY_TODO_EPIC_SENT),
        mergeMap((action)=>api.modifyTodoEpic(action.id,action.text).pipe(
            flatMap(()=>{
                let actions = [{type:MODIFY_TODO,id:action.id},{type:REQUEST_MODIFY_TODO_FULFILLED_EPIC}]
                return from(actions);
            })
        ))
    )
