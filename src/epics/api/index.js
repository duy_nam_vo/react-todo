import {from} from 'rxjs';
import {getTodo,postTodo,deleteTodoData,modifyTodoData} from '../../data/dataAccessLayer';
import 'rxjs/add/observable/fromPromise';

/*
Wrapper for RxJS by bridging promise to Observable
*/
export const api = {
    getTodoEpic:()=>{
        const request = getTodo();
        return from(request);
    },
    postTodoEpic:(data)=>{
        const request = postTodo({"entry":data});
        return from(request);
    },
    deleteTodoEpic:(id)=>{
        const request=deleteTodoData(id);
        return from(request);
    },
    modifyTodoEpic:(id,text)=>{
        const request = modifyTodoData(id,text);
        return from(request);
    }
}