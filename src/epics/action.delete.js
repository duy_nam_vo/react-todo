import { ofType } from 'redux-observable';
import {flatMap,mergeMap} from 'rxjs/operators'
import {api} from './api'
import { from } from 'rxjs';
import {REMOVE_TODO} from '../actionType/index'
export const REQUEST_TODO_DELETE_EPIC_SENT = "REQUEST_TODO_DELETE_EPIC_SENT"
export const REQUEST_TODO_DELETE_FULFILLED = "REQUEST_TODO_DELETE_FULFILLED"



export const deleteTodoEpic = action$=> action$.pipe(
        ofType(REQUEST_TODO_DELETE_EPIC_SENT),
        mergeMap((action)=>api.deleteTodoEpic(action.id).pipe(
            flatMap(()=>{
                let actions = [
                    {type:REMOVE_TODO,id:action.id},
                    {type:REQUEST_TODO_DELETE_FULFILLED}
                ]
                return from(actions);
            })
        ))
);
