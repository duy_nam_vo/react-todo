import {combineEpics} from 'redux-observable';
import {fetchTodoEpic} from './action.fetch';
import {addTodoEpic} from './action.todo.add';
import {connectToSocketIoEpic} from './action.connection';
import {deleteTodoEpic} from './action.delete';
import {modifyTodoEpic} from './action.modify';
const rootEpic = combineEpics(
                             connectToSocketIoEpic,
                             fetchTodoEpic,
                             addTodoEpic,
                             deleteTodoEpic,
                             modifyTodoEpic
                             );
export default rootEpic