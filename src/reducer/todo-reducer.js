import 'babel-polyfill';
import {
    ADD_TODO,
    UPDATE_ENTRY,
    REMOVE_TODO,
    MODIFY_TODO,
    CHANGE_TODO_ENTRY_STATE,
    UPDATE_MODIFY_INPUT,
    UNDO_CHANGE,
    RECEIVE_TODO,
    REQUEST_TODO
} from '../actionType';
import {
    entryState,
    todoState
} from '../static/entryState';
import {
    initialState
} from './model';
import {ADD_TODO_EPIC} from '../epics/action.todo.add';
function appendStateToEntries(entry) {
    return Object.assign({}, {
        entry: entry,
        state: entryState.READ,
        modifyInput: entry
    })
}

function AppTodo(state = initialState, action) {
    let newEntries = null;
    let entries = null;
    switch (action.type) {
        case ADD_TODO_EPIC:
        case ADD_TODO:
            return Object.assign({}, state, {
                entries: [...state.entries, Object.assign({}, {
                    entry: state.input,
                    state: entryState.READ,
                    modifyInput: state.input
                })],
                input: ""
            });
            
        case UPDATE_ENTRY:
            return Object.assign({}, state, {
                input: action.text
            })

        case REMOVE_TODO:
            newEntries = [...state.entries]
            return Object.assign({}, state, {
                entries: newEntries.filter(elem=>elem.id !== action.id)
            })

        case MODIFY_TODO:
            entries = state.entries.map(val=>{
                                if (val.id === action.id){
                                    val.entry = val.modifyInput;
                                    val.state = entryState.READ
                                }
                                return val;
                            });
            return Object.assign({},state,{entries:entries})

        case CHANGE_TODO_ENTRY_STATE:
            newEntries = state.entries[action.id];
            newEntries.state = action.newState;
            if (newEntries.state === entryState.READ) {
                newEntries.modifyInput = newEntries.entry;
            }
            return Object.assign({}, state, {
                entries: Object.assign([], [...state.entries], {
                    [action.id]: newEntries
                })
            })

        case UPDATE_MODIFY_INPUT:
            newEntries = state.entries[action.id];
            newEntries.modifyInput = action.text;
            if (newEntries.modifyInput !== newEntries.entry) {
                newEntries.state = entryState.MODIFIED;
            } else {
                newEntries.state = entryState.MODIFY;
            }
            return Object.assign({}, state, {
                entries: Object.assign([], [...state.entries], {
                    [action.id]: newEntries
                })
            })

        case UNDO_CHANGE:
            newEntries = state.entries[action.id];
            newEntries.modifyInput = newEntries.entry;
            newEntries.state = entryState.MODIFY;
            return Object.assign({}, state, {
                entries: Object.assign([], [...state.entries], {
                    [action.id]: newEntries
                })
            })

        case RECEIVE_TODO:
            newEntries = action.data.todo.map(appendStateToEntries);
            return Object.assign({}, state, {
                todoState: todoState.FETCHED,
                entries: newEntries
            })

        case REQUEST_TODO:
            return Object.assign({}, state, {
                todoState: todoState.FETCHING
            })

        default:
            return state

    }
}

export default AppTodo;