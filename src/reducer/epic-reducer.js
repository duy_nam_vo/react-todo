import {REQUEST_RECEIVED_EPIC,REQUEST_TODO_EPIC,REQUEST_FETCH_FULFILLED_EPIC} from '../epics/action.fetch';
import {ADD_TODO_EPIC} from '../epics/action.todo.add';
const initialState = {
    action:""
}
function AppEpic(state = initialState, action) {
    switch (action.type){
        case REQUEST_RECEIVED_EPIC:
        case REQUEST_TODO_EPIC:
        case ADD_TODO_EPIC:
            return Object.assign({},state,{action:action.type})
        case REQUEST_FETCH_FULFILLED_EPIC:
            return Object.assign({},state,{data:action.data,contentState:action.contentState})
        default:
            return Object.assign({},state,{action:"no action"})
            
    }
}

export default AppEpic;