import { crossSliceReducer } from './crossSlice-reducer';
import {combineReducers} from 'redux';
import AppTodo from './todo-reducer';
import AppConnection from './connection-reducer';
import AppEpic from './epic-reducer';
// import AppSliceReducerA from './slideReducer';

const combinedReducer = combineReducers({AppTodo,AppConnection,AppEpic});

function rootReducer(state, action) {
    const intermediateState = combinedReducer(state, action);
    const finalState = crossSliceReducer(intermediateState, action);
    return finalState;
}

export default rootReducer;