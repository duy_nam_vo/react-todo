import { REQUEST_SENT, REQUEST_RECEIVED,REQUEST_ERROR,
    REQUEST_ADD_ERROR,REQUEST_ADD_RECEIVED,REQUEST_ADD_SENT,
    REQUEST_MODIFY_ERROR,REQUEST_MODIFY_RECEIVED,REQUEST_MODIFY_SENT,
    REQUEST_DELETE_ERROR,REQUEST_DELETE_RECEIVED,REQUEST_DELETE_SENT } 
from "../actionType/crossSlice";
import {
    todoState,
    entryState
} from '../static/entryState';
import {REQUEST_FETCH_FULFILLED_EPIC} from '../epics/action.fetch';
import {REQUEST_ADD_TODO_EPIC,REQUEST_ADD_TODO_FULFILLED_EPIC} from '../epics/action.todo.add';
import {REQUEST_TODO_DELETE_EPIC_SENT,REQUEST_TODO_DELETE_FULFILLED} from '../epics/action.delete';
import {REQUEST_MODIFY_TODO_EPIC_SENT,REQUEST_MODIFY_TODO_FULFILLED_EPIC} from '../epics/action.modify';

export function crossSliceReducer(state, action) {
    let App=null;
    let entries = null;
    
    switch (action.type) {

        case REQUEST_SENT:
        case REQUEST_ADD_SENT:
        case REQUEST_MODIFY_SENT:
        case REQUEST_DELETE_SENT:
        case REQUEST_ADD_TODO_EPIC:
        case REQUEST_TODO_DELETE_EPIC_SENT:
        case REQUEST_MODIFY_TODO_EPIC_SENT:

            App = Object.assign({}, state.AppTodo);
            return Object.assign({}, state, {
                AppTodo: Object.assign({}, App, {
                    todoState: todoState.FETCHING
                })
            });
            
        case REQUEST_ADD_RECEIVED:
        case REQUEST_MODIFY_RECEIVED:
        case REQUEST_DELETE_RECEIVED:
        case REQUEST_ADD_TODO_FULFILLED_EPIC:
        case REQUEST_TODO_DELETE_FULFILLED:
        case REQUEST_MODIFY_TODO_FULFILLED_EPIC:
            App = Object.assign({}, state.AppTodo)
            return Object.assign({}, state, {
                AppTodo: Object.assign({}, App, {
                    todoState: todoState.READ
                })
            })

        case REQUEST_RECEIVED:
        case REQUEST_FETCH_FULFILLED_EPIC:
            entries = action.data.reduce((acc,cur)=>{
                acc.push({
                    id : cur._id,
                    entry : cur.entry,
                    state: entryState.READ,
                    modifyInput: cur.entry
                })
                return acc;
            },[])
            App = Object.assign({}, state.AppTodo)

                return Object.assign({}, state, {
                    AppTodo: Object.assign({}, App, {
                        todoState: todoState.FETCHED,
                        entries : App.entries.concat(entries)
                    })
                })
        
        case REQUEST_ERROR:
        case REQUEST_MODIFY_ERROR:
        case REQUEST_ADD_ERROR:
        case REQUEST_DELETE_ERROR:
                App = Object.assign({}, state.AppTodo)

                return Object.assign({}, state, {
                    AppTodo: Object.assign({}, App, {
                        todoState: todoState.ERROR,
                        error: action.error
                    })
                })
        
        default:
            return state;
    }
}
