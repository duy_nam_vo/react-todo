import {CONNECTION_RECEIVED} from '../actionType/connection';
import {CONNECT_RECEIVED_EPIC,DISCONNECT_RECEIVED_EPIC} from '../epics/action.connection';
const initialState = {
    status:0,
    connectReceivedToState: 0
}
function AppConnection(state = initialState, action) {
    switch (action.type){
        case CONNECTION_RECEIVED:
        case CONNECT_RECEIVED_EPIC:
            // console.log("connection received")
            return Object.assign({},state,{status:1})
        case DISCONNECT_RECEIVED_EPIC:
            return Object.assign({},state,{status:0})
        case "CONNECT_RECEIVED_TO_STATE":
            return Object.assign({},state,{status:1})

        default:
            return state
            
    }
}

export default AppConnection;