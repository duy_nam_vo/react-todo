// import { initialState } from './reducer/model.js';
export const loadState= () =>{
    try{
        const local = localStorage.getItem('state');
        if (local === null){
            return undefined;
        }
        return JSON.parse(local);
    } catch(e){
        return undefined;
    }
}
export const saveState = (state) =>{
    try{
        localStorage.setItem('state',JSON.stringify(state))
    }catch(e){
        console.error(e);
    }
}